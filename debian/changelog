netlib-java (0.9.3-7) unstable; urgency=medium

  [ Dominik Stadler ]
  * Team upload.
  * Add patch to set source to 1.8 as 1.5 is not supported with JDK 11
  * Add patch to fail on generate-task to not hide errors in build-output
    Closes: #923759, #924804
  * Enhance patch to use a fixed list for the classpath to allow building
    on JDK

  [ Andreas Tille ]
  * debhelper-compat 12
  * Standards-Version: 4.4.0
  * Trim trailing whitespace.

 -- Andreas Tille <tille@debian.org>  Wed, 18 Sep 2019 21:18:23 +0200

netlib-java (0.9.3-6) unstable; urgency=medium

  * Drop /build/netlib-java-0.9.3 from URLs

 -- Andreas Tille <tille@debian.org>  Wed, 27 Mar 2019 08:35:16 +0100

netlib-java (0.9.3-5) unstable; urgency=medium

  * Fix URLClassLoader
    Closes: #923759

 -- Andreas Tille <tille@debian.org>  Tue, 26 Mar 2019 16:47:32 +0100

netlib-java (0.9.3-4) unstable; urgency=medium

  * Deactivate watch file since in debian/README.source is declared that
    we do not really want to package the new versions
  * debhelper 12
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.3.0
  * Secure URI in copyright format

 -- Andreas Tille <tille@debian.org>  Sun, 13 Jan 2019 21:11:05 +0100

netlib-java (0.9.3-3) unstable; urgency=medium

  * Add README.source why the old version is kept instead to upgrade to
    the latest upstream version
  * Upload to unstable (main)

 -- Andreas Tille <tille@debian.org>  Sat, 25 Jun 2016 08:46:13 +0200

netlib-java (0.9.3-2) experimental; urgency=low

  * Upload to main since preconditions are in main as well
  * debian/copyright:
     - DEP5
     - new upstream location
  * cme fix dpkg-control
  * debhelper 9
  * Move from Debian Med team to pkg-java team maintenance
  * Add manifest

 -- Andreas Tille <tille@debian.org>  Thu, 14 Apr 2016 14:30:50 +0200

netlib-java (0.9.3-1) unstable; urgency=low

  * Initial release (Closes: #665343)

 -- Olivier Sallou <osallou@debian.org>  Mon, 26 Mar 2012 14:15:20 +0200
